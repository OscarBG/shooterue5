// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerChar.h"
#include "GameFramework/Character.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"



// Sets default values
APlayerChar::APlayerChar()
{
	PrimaryActorTick.bCanEverTick = true;

	cam = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCam"));
	cam->SetupAttachment(GetCapsuleComponent());
	cam->bUsePawnControlRotation = true;

	hingeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("HingeWeapon"));
	hingeMesh->SetupAttachment(cam);
	hingeMesh->bCastDynamicShadow = true;
	hingeMesh->CastShadow = false;
	// hingeMesh->SetRelativeLocation(FVector())
	hingeMesh->SetOnlyOwnerSee(true);

	UCharacterMovementComponent* cmove = GetCharacterMovement();
	cmove->BrakingFriction = 10.f;
	cmove->MaxAcceleration = 10000.f;
	cmove->MaxWalkSpeed = 2000.f;
	cmove->JumpZVelocity = 570.f;
	cmove->AirControl = 2.f;
}

// Called when the game starts or when spawned
void APlayerChar::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void APlayerChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &APlayerChar::OnUseItem);

	PlayerInputComponent->BindAction("Change", IE_Pressed, this, &APlayerChar::OnChange);

	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &APlayerChar::Run);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &APlayerChar::StopRunning);
	
	PlayerInputComponent->BindAxis("Forward", this, &APlayerChar::MoveForward);
	PlayerInputComponent->BindAxis("Right", this, &APlayerChar::MoveRight);

	PlayerInputComponent->BindAxis("MouseRight", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("MouseUp", this, &APawn::AddControllerPitchInput);

}

void APlayerChar::MoveForward(float vel) {
	if (vel != 0)
		AddMovementInput(GetActorForwardVector(), vel * moveSpeed);
		//UE_LOG(LogTemp, Warning, TEXT("adelante"));
}

void APlayerChar::MoveRight(float vel) {
	if (vel != 0)
		AddMovementInput(GetActorRightVector(), vel * moveSpeed);
		//UE_LOG(LogTemp, Warning, TEXT("andelao"));
}

void APlayerChar::Run() {
	UCharacterMovementComponent* cmove = GetCharacterMovement();
	cmove->MaxWalkSpeed = 4000.f;
}

void APlayerChar::StopRunning() {
	UCharacterMovementComponent* cmove = GetCharacterMovement();
	cmove->MaxWalkSpeed = 2000.f;
}

void APlayerChar::OnUseItem(){
	evOnUseItem.Broadcast();
}

void APlayerChar::OnChange() {
	if (WeaponNumber > 1) {
		evOnChange.Broadcast();
	}

}


